﻿function GetPesterTests {
    param (
        [string]$Path
    )

    # Overwrite the Describe function to catch the name
    function Describe {
        param (
            [string]$name,
            [scriptblock]$scriptblock
        )
        
        # Set the current Describe
        $current.Describe = $name
        
        # Execute the Describe codeblock
        Invoke-Command -ScriptBlock $scriptblock
        
        # Reset the current Describe
        $current.Describe = ''
    }

    # Overwrite the Context function to catch the name
    function Context {
        param (
            [string]$name,
            [scriptblock]$scriptblock
        )
        
        # Set the current Context
        $current.Context = $name
        
        # Execute the Context codeblock
        Invoke-Command -ScriptBlock $scriptblock -ArgumentList $name
        $current.Context = ''
    }

    # Overwrite the It function to catch the name
    function It {
        param (
            [string]$name,
            [scriptblock]$scriptblock
        )
        
        # Set the current It
        $current.It = $name
        
        # Create the test object and add it to the array
        $test = New-Object -TypeName psobject -Property $current
        $testArray.Add($test) | Out-Null
        
        # Execute the It codeblock (not really needed)
        #Invoke-Command -ScriptBlock $scriptblock
        
        # Reset the current It
        $current.It = ''
    }

    # Make sure Should is ignored
    function Should {}

    function Mock {}

    function BeforeAll {}

    # Create array for storing all the returning objects
    $testArray = New-Object -TypeName System.Collections.ArrayList
    
    # Properties for each object which will contain one It with its Describe and Context
    $current = @{
        Describe = ''
        Context = ''
        It = ''
    }

    # Get the test script and execute it
    $script = Get-Command $Path | Select-Object -ExpandProperty ScriptBlock 
    #Invoke-command -ScriptBlock $script
    & $Path
    # Return the array
    return $testArray
}

GetPesterTests -Path C:\git\BitBucket_code\Get-ParamHelp.tests.ps1
