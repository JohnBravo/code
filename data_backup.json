[
  { 
    "Name" : "ValueFromPipeline",
    "Type" : "Argument",
    "Parent" : "Parameter",
    "Description" : "The ValueFromPipeline argument indicates that the parameter accepts input from a pipeline object. Specify this argument if the function accepts the entire object, not just a property of the object.",
    "Examples" : [
      {
          "Id" : 1,
          "Example" : "[parameter(ValueFromPipeline=$true)]",
          "Description" : "Enabled"
      },
      {
        "Id" : 2,
        "Example" : "[parameter(ValueFromPipeline=$false)]",
        "Description" : "Disabled (default)"
      }
    ],
    "Links" : [
      {
          "Uri" : "https://msdn.microsoft.com/en-us/library/system.management.automation.parameterattribute.valuefrompipeline(v=vs.85).aspx",
          "Description" : "ParameterAttribute.ValueFromPipeline Property (MSDN)"
      }
    ] 
  },
  { 
    "Name" : "ValueFromPipelineByPropertyName",
    "Type" : "Argument",
    "Parent" : "Parameter",
    "Description" : "The valueFromPipelineByPropertyName argument indicates that the parameter accepts input from a property of a pipeline object. The object property must have the same name or alias as the parameter.",
    "Examples" : [
      {
          "Id" : 1,
          "Example" : "[parameter(ValueFromPipelineByPropertyName=$true)]",
          "Description" : "Enabled"
      },
      {
        "Id" : 2,
        "Example" : "[parameter(ValueFromPipelineByPropertyName=$false)]",
        "Description" : "Disabled (default)"
      }
    ],
    "Links" : [
      {
          "Uri" : "https://msdn.microsoft.com/en-us/library/system.management.automation.parameterattribute.valuefrompipelinebypropertyname(v=vs.85).aspx",
          "Description" : "ParameterAttribute.ValueFromPipelineByPropertyName Property (MSDN)"
      }
    ]
  },
  { 
    "Name" : "Parameter",
    "Type" : "Attribute",
    "Parent" : "",
    "Description" : "The Parameter attribute is used to declare the attributes of function parameters.",
    "Examples" : [
      {
          "Id" : 1,
          "Example" : "[parameter(Argument=Value)]",
          "Description" : "Declares validation argument for parameter."
      }
    ],
    "Links" : [
      {
          "Uri" : "https://msdn.microsoft.com/en-us/library/ms714348(v=vs.85).aspx",
          "Description" : "Parameter Attribute Declaration (MSDN)"
      },
      {
          "Uri" : "https://msdn.microsoft.com/en-us/library/system.management.automation.parameterattribute(v=vs.85).aspx",
          "Description" : "ParameterAttribute Class (MSDN)"
      }
    ]
  },
  { 
    "Name" : "Mandatory",
    "Type" : "Argument",
    "Parent" : "Parameter",
    "Description" : "Makes a parameter mandatory.",
    "Examples" : [
      {
          "Id" : 1,
          "Example" : "[parameter(Mandatory=$true)]",
          "Description" : "Makes parameter mandatory"
      },
      {
          "Id" : 2,
          "Example" : "[parameter(Mandatory=$false)]",
          "Description" : "Makes parameter optional (default)"
      }
    ],
    "Links" : [
      {
          "Uri" : "https://msdn.microsoft.com/en-us/library/ms714348(v=vs.85).aspx",
          "Description" : "Parameter Attribute Declaration (MSDN)"
      },
      {
          "Uri" : "https://msdn.microsoft.com/en-us/library/system.management.automation.parameterattribute.mandatory(v=vs.85).aspx",
          "Description" : "ParameterAttribute.Mandatory Property (MSDN)"
      }
    ]
  },
  { 
    "Name" : "Position",
    "Type" : "Argument",
    "Parent" : "Parameter",
    "Description" : "Sets the position of a parameter if the name is not used.",
    "Examples" : [
      {
          "Id" : 1,
          "Example" : "[parameter(Position=0)]",
          "Description" : "Sets the position to the first parameter if parameter name is omitted."
      }
    ],
    "Links" : [
      {
          "Uri" : "https://msdn.microsoft.com/en-us/library/ms714348(v=vs.85).aspx",
          "Description" : "Parameter Attribute Declaration (MSDN)"
      }
    ]
  },
  { 
    "Name" : "ParameterSetName",
    "Type" : "Argument",
    "Parent" : "Parameter",
    "Description" : "Defines which parameter set a parameter belongs to. Multiple sets can be used. Parameters belonging to other sets will be filtered out if a set parameter is selected.",
    "Examples" : [
      {
          "Id" : 1,
          "Example" : "[parameter(ParameterSetName=\"MySetName\")]",
          "Description" : "Sets the parameter set name to \"MySetName\"."
      }
    ],
    "Links" : [
      {
          "Id" : 2,
          "Uri" : "https://msdn.microsoft.com/en-us/library/ms714348(v=vs.85).aspx",
          "Description" : "Parameter Attribute Declaration (MSDN)"
      }
    ]
  }
]